﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class esfera : MonoBehaviour {
	//Tarea entregable del 11 de octubre

	//Creo el rango en la esfera
	[Range(0, 10)] public float shaderColor = 0;


	//aviso que hay un gameobject y un mesh renderer al que voy a acceder
	GameObject cuboGO;
	MeshRenderer cuboGOrenderer;

	// Use this for initialization
	void Start () {

		//los "lleno"
		cuboGO = GameObject.Find ("cubito");
		cuboGOrenderer=cuboGO.GetComponent<MeshRenderer> ();
		
	}
	
	// Update is called once per frame
	void Update () {

		//dentro del material hay un float que voy a ir cambiando
		cuboGOrenderer.material.SetFloat ("_MySlider", shaderColor);
	}
}
