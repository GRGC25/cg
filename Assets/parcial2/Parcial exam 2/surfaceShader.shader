﻿Shader "Examen-SurfaceShader" {

	Properties {


		_Color ("Color", Color) = (1,1,1,1)
		_AmbientColor("AmbientColor",Color)=(1,1,1,1)
		_MySlider ("Mi slider", Range (0,10))=0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		fixed4 _AmbientColor;
		float _MySlider;
		UNITY_INSTANCING_CBUFFER_START(Props)
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {


			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color+((_MySlider*_AmbientColor));
			o.Albedo = c.rgb;

			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

