﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class examen : MonoBehaviour {
	MeshRenderer myRenderer;

	float counter=0;
	float surfaceSlide=0;
	float normalIntensity=0;

	//canvas
	GameObject tiempoTextoGO;
	Text tiempoTexto;

	GameObject shaderTextoGO;
	Text shaderTexto;

	GameObject colorTextoGO;
	Text colorTexto;

	GameObject luzTextoGO;
	Text luzTexto;

	GameObject texturaTextoGO;
	Text texturaTexto;


	//camara
	GameObject camara;
	Camera camaraFOV;
	//luz
	GameObject lightGO;
	Light lightEnabler;

	//color
	Color mycolor;


	//texturas en c#
	public Texture2D normalSolo;
	public Texture2D albedoSolo;
	public Texture2D normalJunto;
	public Texture2D albedoJunto;

	// Use this for initialization
	void Start () {
		
		//material
		myRenderer = GetComponent<MeshRenderer> ();


		//camara
		camara = GameObject.Find ("Main Camera");
		camaraFOV = camara.GetComponent<Camera> ();

		//textos
		tiempoTextoGO= GameObject.Find ("Canvas/Tiempo");
		tiempoTexto = tiempoTextoGO.GetComponent<Text> ();

		shaderTextoGO = GameObject.Find ("Canvas/Shader");
		shaderTexto = shaderTextoGO.GetComponent<Text> ();

		colorTextoGO= GameObject.Find ("Canvas/color");
		colorTexto = colorTextoGO.GetComponent<Text> ();

		luzTextoGO= GameObject.Find ("Canvas/luz");
		luzTexto = luzTextoGO.GetComponent<Text> ();

		texturaTextoGO= GameObject.Find ("Canvas/textura");
		texturaTexto = texturaTextoGO.GetComponent<Text> ();


		//luz
		lightGO=GameObject.Find("Directional Light");
		lightEnabler = lightGO.GetComponent<Light>();

		lightEnabler.enabled = lightEnabler != enabled;
		luzTexto.text = "light: off";


		//color inicial

	

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		counter +=Time.deltaTime;
		tiempoTexto.text = "tiempo " + counter;
		myRenderer.material.SetColor ("_Color", mycolor);



		//shader 1
		if (counter <= 5) {

			mycolor = Color.black;
			myRenderer.material.shader = Shader.Find ("Examen-Diffuse");
			shaderTexto.text = "shader: Diffuse";
			texturaTexto.text = " Textura: off";
			colorTexto.text = "Color: " + mycolor;
		} 
			
		//shader 2
		else if (counter <= 9) {
			mycolor = Color.magenta;
			surfaceSlide = surfaceSlide + 0.002f;
			myRenderer.material.shader = Shader.Find ("Examen-SurfaceShader");
			//myRenderer.material.SetFloat ("_MySlider", surfaceSlide);

			shaderTexto.text = "shader: surface + ambient";
			luzTexto.text = "light: on";
			colorTexto.text = "Color: " + mycolor;

		} 
		else if (counter <= 15) {
			mycolor = Color.magenta;
			surfaceSlide = surfaceSlide + 0.005f;
			myRenderer.material.shader = Shader.Find ("Examen-SurfaceShader");
			myRenderer.material.SetFloat ("_MySlider", surfaceSlide);

			shaderTexto.text = "shader: surface + ambient";
			lightEnabler.enabled = true;
			luzTexto.text = "light: on";
			colorTexto.text = "Color: " + mycolor;
		}

		//shader3
		else if (counter <= 30) {
			myRenderer.material.shader = Shader.Find ("Examen-texturaScrolling");
			myRenderer.material.SetTexture ("_MainTex", albedoSolo);
			shaderTexto.text = "shader: Textura simple";
			texturaTexto.text = " Textura: on";
			lightEnabler.enabled = true;
		} 
		//shader 4
		else if (counter <= 45) {
			//me aseguro de que el fov no se movera hasta que acabe este else if
			camaraFOV.fieldOfView = 60;
			if (normalIntensity <= 1) {
				normalIntensity += Time.deltaTime;
			}
			myRenderer.material.shader = Shader.Find ("Examen-NormalMapScrolling");
			myRenderer.material.SetTexture ("_NormalMap", normalSolo);
			myRenderer.material.SetFloat ("_NormalMapIntensity", normalIntensity);
			shaderTexto.text = "shader: Normal map";
			lightEnabler.enabled = true;

		} 
		//shader 5
		else if (counter <= 60) {
	
			myRenderer.material.shader = Shader.Find ("Examen-NormalTexturaScrolling");
			myRenderer.material.SetTexture ("_MainTex", albedoJunto);
			myRenderer.material.SetTexture ("_NormalMap", normalJunto);
			lightEnabler.enabled = true;

			if(camaraFOV.fieldOfView  >=20){
				
				camaraFOV.fieldOfView = camaraFOV.fieldOfView - Time.deltaTime*15;

				shaderTexto.text = "shader: Normal map + textura";

			}
		}
		//de regreso al inicio
		else {
			
			camaraFOV.fieldOfView = 60;
			counter = 0;
			surfaceSlide = 0;
			float normalIntensity=0;
		}
		
	}
}
