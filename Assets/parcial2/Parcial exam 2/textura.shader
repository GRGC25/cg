﻿Shader "Examen-texturaScrolling" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ScrollXSpeed("x scroll speed",Range(-10,10))=1
		_ScrollYSpeed("y scroll speed",Range(-10,10))=0.2

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0



		struct Input {
			float2 uv_MainTex;
		};

		sampler2D _MainTex;
		fixed _ScrollYSpeed, _ScrollXSpeed;

		UNITY_INSTANCING_CBUFFER_START(Props)
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {

		fixed2 scrolledUV = IN.uv_MainTex;
		fixed ScrollYValue= _ScrollYSpeed *_Time;
		fixed ScrollXValue= _ScrollXSpeed *_Time;

		scrolledUV += fixed2 ( ScrollXValue,ScrollYValue);
		half4 c= tex2D(_MainTex, scrolledUV);
		o.Albedo =c.rgb;
		o.Alpha =c.a;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
