﻿Shader "Examen-Diffuse" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
		//lo dejo porque si no, no compila
		float2 uv_MainTex;
		};


		fixed4 _Color;
		UNITY_INSTANCING_CBUFFER_START(Props)
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutput o) {
		o.Albedo=_Color.rgb;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
