﻿Shader "Examen-NormalTexturaScrolling" {
	Properties {
		
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap (" Normal map", 2D) = "bump"{}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM

		#pragma surface surf Lambert 
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		float _NormalMapIntensity;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};
	

		void surf (Input IN, inout SurfaceOutput o) {


		fixed ScrollYValue= 0.2*_Time;
		fixed ScrollXValue= 1*_Time;


		fixed2 scrolledNM =IN.uv_NormalMap;
		fixed2 scrolledMT =IN.uv_MainTex;

		scrolledNM += fixed2 ( ScrollXValue,ScrollYValue);
		scrolledMT += fixed2 ( ScrollXValue,ScrollYValue);

		o.Albedo =tex2D(_MainTex,scrolledMT).rgb;
		float3 normalMap = UnpackNormal(tex2D(_NormalMap,scrolledNM));


		o.Normal=normalize(normalMap);

		}
		ENDCG
	}
	FallBack "Diffuse"
}
