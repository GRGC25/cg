﻿Shader "Examen-NormalMapScrolling" {
Properties {
		
		_NormalMap (" Normal map", 2D) = "bump"{}
		_NormalMapIntensity("Normal Intensity" , Range(0,1))=0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM

		#pragma surface surf Lambert 

		#pragma target 3.0

		sampler2D _NormalMap;
		float _NormalMapIntensity;

		struct Input {
			float2 uv_NormalMap;
		};
	

		void surf (Input IN, inout SurfaceOutput o) {
		//color blanco general
		o.Albedo=fixed3(1,1,1);

		//guardo el normal map en otra variable
		fixed2 scrolledNM = IN.uv_NormalMap;

		//preparo el scrolling
		fixed ScrollYValue= 1*_Time;
		fixed ScrollXValue= 0 *_Time;

		scrolledNM += fixed2 ( ScrollXValue,ScrollYValue);


		//preparo el normal map con scrolling
		float3 normalMap = UnpackNormal(tex2D(_NormalMap,scrolledNM));

		//acomodo la intensidad
		normalMap.x *=_NormalMapIntensity;
		normalMap.y *=_NormalMapIntensity;

		//muestro el resultado
		o.Normal=normalize(normalMap);

		}
		ENDCG
	}
	FallBack "Diffuse"
}
