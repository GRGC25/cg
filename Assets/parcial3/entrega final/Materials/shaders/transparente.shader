﻿Shader "proyectoFinal/transparente" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}

	SubShader {
	Cull Off

		Tags { "RenderType"="Transparent"
		//deberia ser transparent. pero el punto es cambiar los tags para cambiar efectos en los shaders
		//"Queue"="Opaque"
		"Queue"="Transparent"
		"IgnoreProjector"="True"
		  }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
		float4 c= tex2D(_MainTex,IN.uv_MainTex)*_Color;
		o.Albedo=c.rgb;
		o.Alpha=c.a;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
