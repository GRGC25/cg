﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class texto : MonoBehaviour {
	GameObject cuadroTexto; 
	GameObject fondo;
	Image fondoColor;
	Text letrasTexto;
	float contador=0;

	// Use this for initialization
	void Start () {

		cuadroTexto = GameObject.Find("Canvas/Text");
		fondo = GameObject.Find ("Canvas/Image");

		letrasTexto= cuadroTexto.GetComponent<Text>();
		fondoColor = fondo.GetComponent<Image> ();


	}
	
	// Update is called once per frame
	void FixedUpdate () {
		contador = contador + Time.deltaTime;
		if (contador <= 2) {

			letrasTexto.text = "transparente (alambre)";
			fondoColor.CrossFadeAlpha (1, 0, true);
		} else if (contador <= 5.5) {

			letrasTexto.text = "Diffuse + Normal map (casa)";
		} else if (contador <= 9) {

			letrasTexto.text = "Diffuse sencillo (hojas de arbol) | Diffuse + Normal map (tronco)";
		} else if (contador <= 11.5) {

			letrasTexto.text = "Diffuse + Normal map (calle y piso)";

		}
		else if (contador <= 20) {
			letrasTexto.text = "";
			fondoColor.CrossFadeAlpha (0, .2f, true);

		} 

		else if (contador <= 23) {

			letrasTexto.text = "Phong (faro) ";
			fondoColor.CrossFadeAlpha (1, .2f, true);
		} 

		else if (contador <= 25) {
			letrasTexto.text = "";
			fondoColor.CrossFadeAlpha (0, .2f, true);
		} 

		else if (contador <= 29) {
			letrasTexto.text = "Diffuse + Normal map (casa)";
			fondoColor.CrossFadeAlpha (1, .2f, true);
		} 



		else if (contador <= 33) {
			letrasTexto.text = "";
			fondoColor.CrossFadeAlpha (0, .2f, true);
		} 

		else if (contador <= 36) {
			letrasTexto.text = "Diffuse + Normal map (chimenea)";
			fondoColor.CrossFadeAlpha (1, .2f, true);
		} 

		else if (contador <= 41) {
			letrasTexto.text = "";
			fondoColor.CrossFadeAlpha (0, .2f, true);
		} 

		else if (contador <= 44) {
			letrasTexto.text = "Diffuse sencillo (hojas de arbol) | Diffuse + Normal map (tronco)";
			fondoColor.CrossFadeAlpha (1, .2f, true);
		} 

		else if (contador <= 49) {
			letrasTexto.text = "";
			fondoColor.CrossFadeAlpha (0,.2f, true);
		} 

		else if (contador <= 53) {
			letrasTexto.text = "procedural ramp (cajas)";
			fondoColor.CrossFadeAlpha (1, .2f, true);
		} 
		else
		{
			letrasTexto.text = " ";
			fondoColor.CrossFadeAlpha (0, .2f, true);
		}


	}
}
